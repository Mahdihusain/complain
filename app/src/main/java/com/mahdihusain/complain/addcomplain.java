package com.mahdihusain.complain;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mahdihusain on 29/03/2016.
 */
public class addcomplain extends AppCompatActivity{


    EditText complain;
    RequestQueue requestQueue;
    RelativeLayout addcomplainscreen;
    EditText complaintitle;
    CheckBox checkboxmine;
    CheckBox checkboxhostel;
    CheckBox checkboxiit;

    String userid;
    String hostel;
    int complaintype;


//onCreate addComplainView
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addcomplain);

        userid = User.getInstance().getUserid();
        hostel = User.getInstance().getHostel();

        requestQueue = Volley.newRequestQueue(this);


        checkboxmine = (CheckBox) this.findViewById(R.id.checkBoxmineid);
        checkboxhostel = (CheckBox) this.findViewById(R.id.checkBox2hostelid);
        checkboxiit = (CheckBox) this.findViewById(R.id.checkBox3iitid);

        //login listener
        final Button post = (Button) findViewById(R.id.buttonpostid);

        post.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(checkboxmine.isChecked()){
                    complaintype = 0;
                }else if(checkboxhostel.isChecked()){
                    complaintype = 1;
                }else {
                    complaintype = 2;
                }


                post.setBackgroundColor(getResources().getColor(R.color.lightgreen));
                onClickedpost(v);
            }
        });

    }

     ///on clicking post button
    public void onClickedpost(View v){

        url urlObj = new url();
        String urlpost = urlObj.addcomplainurl;

        complain = (EditText) findViewById(R.id.complainid);
        complaintitle = (EditText) findViewById(R.id.complaintitleid);

        addcomplainscreen = (RelativeLayout) findViewById(R.id.addcomplainscreenid);


        final String complainstr = complain.getText().toString().trim();
        final String complaintitlestr = complaintitle.getText().toString().trim();



        StringRequest stringRequest = new StringRequest(Request.Method.POST,urlpost,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        if(response.toString().trim().equals("success")){


                            Intent intent = new Intent(addcomplain.this, whatsappview.class);
                            startActivity(intent);

                        }else{
                            snackBarError("Server Error", "Retry", addcomplainscreen);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(addcomplain.this, error.toString(), Toast.LENGTH_SHORT).show();
                        snackBarError("Network Error", "Retry", addcomplainscreen);
                        Log.e("VOLLEY", "AuthFailureError");
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<String,String>();
                params.put("userid", userid);
                if(complaintype==1){
                    params.put("type",hostel);
                }else if(complaintype==0){
                    params.put("type", "Student");
                }else{
                    params.put("type", "iit");
                }
                params.put("complain",complainstr);
                params.put("ComplainTitle",complaintitlestr);


                return params;
            }
        };


        requestQueue.add(stringRequest);

    }


    //snack bar error reporting

    public void snackBarError (String text,String action,RelativeLayout layout ){

        Snackbar snackbar = Snackbar
                .make(layout, text, Snackbar.LENGTH_LONG)
                .setAction(action, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        onClickedpost(view);

                    }
                });

        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(getResources().getColor(R.color.lightgreen));

        snackbar.show();
    }


}

package com.mahdihusain.complain;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.HashMap;
import java.util.Map;


public class Login extends AppCompatActivity {

    RelativeLayout loginscreen;
    CheckBox checkBoxPassword;
    EditText name;
    EditText password;
    Button login;
    RadioButton radiobutton;
    RequestQueue requestQueue;
    boolean isadmin;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //cookie/////
        CookieManager manager = new CookieManager(null, CookiePolicy.ACCEPT_ALL);
        CookieHandler.setDefault(manager);

        requestQueue = Volley.newRequestQueue(this);

        radiobutton = (RadioButton) findViewById(R.id.radiobuttonid);


        checkBoxPassword = (CheckBox) findViewById(R.id.checkboxid);

        //checkBox listener
        checkBoxPassword.setOnClickListener(new CheckBox.OnClickListener() {

            @Override
            public void onClick(View v) {

                password = (EditText) findViewById(R.id.passwordid);

                if (checkBoxPassword.isChecked()) {
                    if (password != null) {
                        password.setTransformationMethod(null);
                    }
                } else {
                    password.setTransformationMethod(new PasswordTransformationMethod());

                }
            }
        });

        //login listener
        login = (Button) findViewById(R.id.loginbuttonid);

        login.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {

                login.setBackgroundColor(getResources().getColor(R.color.lightgreen));


                onClickedLogin(v);
            }
        });


    }



    //on click login button
    public void onClickedLogin(View v){

        url urlObj = new url();
        String urlLogin = urlObj.urlLogin;
      //  String urlLogin = "http://10.202.141.248/complain/login1.php";

        name = (EditText) findViewById(R.id.nameid);
        password = (EditText) findViewById(R.id.passwordid);
        loginscreen = (RelativeLayout) findViewById(R.id.loginscreen);


        final String nameStr = name.getText().toString().trim();
        final String passwordStr = password.getText().toString().trim();

        User.getInstance().setUserid(nameStr);

        saveHostelName();


        radiobutton = (RadioButton) findViewById(R.id.radiobuttonid);

        specialuser suObj = new specialuser();

        isadmin = radiobutton.isChecked();

        if(isadmin){

            if(suObj.adminid.equals(nameStr)){

                StringRequest stringRequest = new StringRequest(Request.Method.POST,urlLogin,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                //Toast.makeText(Login.this, response, Toast.LENGTH_SHORT).show();

                                if(response.toString().trim().equals("success")){

                                    //getStudentComplain();
                                    //getHostelComplain();
                                    //getIITComplain();

                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            Intent intent = new Intent(Login.this, whatsappview.class);
                                            startActivity(intent);
                                        }
                                    }, 5000);

                                }else{
                                    snackBarError("Wrong entry", "Retry", loginscreen);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                //Toast.makeText(Login.this,error.toString(), Toast.LENGTH_SHORT).show();
                                snackBarError("Network Error", "Retry", loginscreen);
                                Log.e("VOLLEY", "AuthFailureError");
                            }
                        }
                ){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> params = new HashMap<String,String>();
                        params.put("userid", nameStr);
                        params.put("pswd", passwordStr);

                        return params;
                    }
                };

                //stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                //      5000,
                //       DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                //     DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


                requestQueue.add(stringRequest);

            }
            else{
                snackBarError("U are not Admin", "Retry", loginscreen);
            }
        }else{


            StringRequest stringRequest = new StringRequest(Request.Method.POST,urlLogin,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            //Toast.makeText(Login.this, response, Toast.LENGTH_SHORT).show();

                            if(response.toString().trim().equals("success")){

                                //getStudentComplain();
                                //getHostelComplain();
                                //getIITComplain();

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent intent = new Intent(Login.this, whatsappview.class);
                                        startActivity(intent);
                                    }
                                }, 5000);

                            }else{
                                snackBarError("Wrong entry", "Retry", loginscreen);
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //Toast.makeText(Login.this,error.toString(), Toast.LENGTH_SHORT).show();
                            snackBarError("Network Error", "Retry", loginscreen);
                            Log.e("VOLLEY", "AuthFailureError");
                        }
                    }
            ){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String,String>();
                    params.put("userid", nameStr);
                    params.put("pswd", passwordStr);

                    return params;
                }
            };

            //stringRequest.setRetryPolicy(new DefaultRetryPolicy(
            //      5000,
            //       DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            //     DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


            requestQueue.add(stringRequest);


        }


    }

    //get complain from server
    public void saveHostelName() {

        url urlObj = new url();
        String hostelurl = urlObj.hostelurl;
        //String hostelurl = "http://10.202.141.248/complain/gethostel.php";


        final String userid = User.getInstance().getUserid();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, hostelurl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        //Toast.makeText(Login.this, response + "hostelset" + userid, Toast.LENGTH_LONG).show();

                        User.getInstance().setHostel(response);

                        getStudentComplain();
                        getHostelComplain();
                        getIITComplain();


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(Login.this, error.toString() + "hostelset", Toast.LENGTH_LONG).show();
                        //snackBarError("Network Error", "Retry", loginscreen);
                        Log.e("VOLLEY", "AuthFailureError");
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("userid", userid);

                return params;
            }
        };

        requestQueue.add(stringRequest);

    }
    //get complain from server
    public void getStudentComplain(){
        url urlObj = new url();
        String urlstudentcomplain = urlObj.urlstudentcomplain;

        //String urlstudentcomplain = "http://10.202.141.248/complain/studentcomplains.php";


        final String userid = User.getInstance().getUserid();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, urlstudentcomplain,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        //Toast.makeText(Login.this, response, Toast.LENGTH_LONG).show();
                        StudentComplain.getInstance().setStudentcomplains(response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(Login.this,error.toString(), Toast.LENGTH_LONG).show();
                        //snackBarError("Network Error", "Retry", loginscreen);
                        Log.e("VOLLEY", "AuthFailureError");
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<String,String>();
                params.put("userid", userid);

                return params;
            }
        };

        //stringRequest.setRetryPolicy(new DefaultRetryPolicy(
        //      5000,
        //       DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
        //     DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        requestQueue.add(stringRequest);
    }



    //get complain from server
    public void getIITComplain(){
        url urlObj = new url();
        String urliitcomplain = urlObj.urliitcomplain;

        //String urliitcomplain = "http://10.202.141.248/complain/iitcomplains.php";


        //final String userid = User.getInstance().getUserid();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, urliitcomplain,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        //Toast.makeText(Login.this, response, Toast.LENGTH_LONG).show();
                          IITComplain.getInstance().setIitcomplain(response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                       // Toast.makeText(Login.this,error.toString(), Toast.LENGTH_LONG).show();
                        //snackBarError("Network Error", "Retry", loginscreen);
                        Log.e("VOLLEY", "AuthFailureError");
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<String,String>();

                return params;
            }
        };

        //stringRequest.setRetryPolicy(new DefaultRetryPolicy(
        //      5000,
        //       DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
        //     DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        requestQueue.add(stringRequest);
    }


    //get complain from server
    public void getHostelComplain(){
        url urlObj = new url();
        String urlhostelcomplain = urlObj.urlhostelcomplain;

        //String urlstudentcomplain = "http://10.202.141.248/complain/hostelcomplaints.php";


        final String userid = User.getInstance().getUserid();
        final String userhostel = User.getInstance().getHostel();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, urlhostelcomplain,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        //Toast.makeText(Login.this, response + "hostelcomplain" + userhostel + "hostelnotapperaing", Toast.LENGTH_LONG).show();
                        HostelComplain.getInstance().setHostelComplain(response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(Login.this,error.toString() + "hostelcomplain", Toast.LENGTH_LONG).show();
                        //snackBarError("Network Error", "Retry", loginscreen);
                        Log.e("VOLLEY", "AuthFailureError");
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<String,String>();
                params.put("userid", userid);
                params.put("hostel",userhostel);    ///change vindy to hostel
                return params;
            }
        };

        //stringRequest.setRetryPolicy(new DefaultRetryPolicy(
        //      5000,
        //       DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
        //     DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        requestQueue.add(stringRequest);
    }

    public void snackBarError (String text,String action,RelativeLayout layout ){

        Snackbar snackbar = Snackbar
                .make(layout, text, Snackbar.LENGTH_LONG)
                .setAction(action, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        onClickedLogin(view);

                    }
                });

        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(getResources().getColor(R.color.lightgreen));

        snackbar.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

package com.mahdihusain.complain;

public class User {

    private static User instance;

    private User()
    {

    }

    public static User getInstance()
    {
        if (instance == null)
            instance = new User();

        return instance;
    }


    String userid;
    String hostel;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getHostel() {
        hostelparse hostelparseObj = new hostelparse(hostel.trim());

        String hot = hostelparseObj.getMyHostel();

        return hot;
    }

    public void setHostel(String hostel) {
        this.hostel = hostel;
    }
}



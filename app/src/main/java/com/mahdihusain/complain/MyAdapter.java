package com.mahdihusain.complain;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private ArrayList<card> cards;
    RequestQueue requestQueue;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public View view;
        public ViewHolder(View v) {
            super(v);
            view = v;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapter(ArrayList<card> cards) {
        this.cards = cards;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        TextView title = (TextView) holder.view.findViewById(R.id.titleid);
        TextView complain = (TextView) holder.view.findViewById(R.id.complainid);
        TextView name = (TextView) holder.view.findViewById(R.id.personnameid);
        final Button likebutton = (Button) holder.view.findViewById(R.id.likeid);
        final TextView nolike = (TextView) holder.view.findViewById(R.id.nolikeid);
        final RadioButton resolve = (RadioButton) holder.view.findViewById(R.id.resolveid);


        //like listener
        likebutton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {

                //likebutton.setBackgroundColor(R.color.darkred);
                int no = Integer.parseInt(nolike.getText().toString()) + 1;
                nolike.setText(Integer.toString(no));
            }
        });

        //radio button listener
        resolve.setOnClickListener(new RadioButton.OnClickListener() {
            @Override
            public void onClick(View v) {

                //likebutton.setBackgroundColor(R.color.darkred);

            }

        });



        title.setText(cards.get(position).getTitle());
        complain.setText(cards.get(position).getComplain());
        name.setText(cards.get(position).getName());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return cards.size();
    }
}
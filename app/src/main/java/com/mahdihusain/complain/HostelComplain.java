package com.mahdihusain.complain;

/**
 * Created by Mahdihusain on 28/03/2016.
 */
public class HostelComplain {

    private static HostelComplain instance;

    private HostelComplain()
    {

    }

    public static HostelComplain getInstance()
    {
        if (instance == null)
            instance = new HostelComplain();

        return instance;
    }


    String hostelComplain;

    public String getHostelComplain() {
        return hostelComplain;
    }

    public void setHostelComplain(String hostelComplain) {
        this.hostelComplain = hostelComplain;
    }
}

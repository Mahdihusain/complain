package com.mahdihusain.complain;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Mahdihusain on 28/03/2016.
 */
public class Json {

    public String removeSlash(String jsonObj){

        Pattern p = Pattern.compile("\\\\");
        // get a matcher object
        Matcher m = p.matcher(jsonObj);
        jsonObj = m.replaceAll("");

        return  jsonObj;
    }


}

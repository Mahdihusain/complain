package com.mahdihusain.complain;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;


public class resolveapi extends AppCompatActivity{


    RequestQueue requestQueue = Volley.newRequestQueue(this);

    public void callresolveAPI(){

        String urlLogin = "http://10.202.141.248/complain/resolve.php";

        StringRequest stringRequest = new StringRequest(Request.Method.POST,urlLogin,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                        //snackBarError("Network Error", "Retry", loginscreen);
                        Log.e("VOLLEY", "AuthFailureError");
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<String,String>();

                return params;
            }
        };

        //stringRequest.setRetryPolicy(new DefaultRetryPolicy(
        //      5000,
        //       DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
        //     DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        requestQueue.add(stringRequest);

    }

}

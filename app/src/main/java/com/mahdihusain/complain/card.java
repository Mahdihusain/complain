package com.mahdihusain.complain;


public class card {

    String title;
    String complain;
    String name;
    String resolve;
    String like;
    String comment;
    String nolike;
    String nocomment;

    public card(String title, String complain, String name) {
        this.title = title;
        this.complain = complain;
        this.name = name;
        this.resolve = resolve;

    }

    public String getTitle() {
        return title;
    }

    public String getComplain() {

        return complain;
    }

    public String getName() {
        return name;
    }

    public String getResolve() {
        return resolve;
    }

    public String getLike() {
        return like;
    }

    public String getComment() {
        return comment;
    }

    public String getNolike() {
        return nolike;
    }

    public String getNocomment() {
        return nocomment;
    }

    public void setResolve(String resolve) {
        this.resolve = resolve;
    }
}

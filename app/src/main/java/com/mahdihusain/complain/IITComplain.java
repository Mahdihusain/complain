package com.mahdihusain.complain;


public class IITComplain {

    private static IITComplain instance;

    private IITComplain()
    {

    }

    public static IITComplain getInstance()
    {
        if (instance == null)
            instance = new IITComplain();

        return instance;
    }

    String iitcomplain;

    public String getIitcomplain() {
        return iitcomplain;
    }

    public void setIitcomplain(String iitcomplain) {
        this.iitcomplain = iitcomplain;
    }
}

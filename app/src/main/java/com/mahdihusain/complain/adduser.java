package com.mahdihusain.complain;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.HashMap;
import java.util.Map;

public class adduser extends AppCompatActivity {

    Button adduserbutton;
    EditText name;
    EditText password;
    EditText Hostel;
    EditText Type;
    RelativeLayout adduserscreen;
    RequestQueue requestQueue;

    //on create add user view
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.adduser);

        requestQueue = Volley.newRequestQueue(this);
        //login listener
        adduserbutton = (Button) findViewById(R.id.adduserbuttonid);

        adduserbutton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {

                adduserbutton.setBackgroundColor(getResources().getColor(R.color.lightgreen));


                onClickedUseraddbutton(v);
            }
        });

    }


    //on click login button
    public void onClickedUseraddbutton(View v) {

        url urlObj = new url();
        String urlLogin = urlObj.adduserurl;

        name = (EditText) findViewById(R.id.newnameid);
        password = (EditText) findViewById(R.id.newpasswordid);
        Hostel = (EditText) findViewById(R.id.hostelid);
        Type = (EditText) findViewById(R.id.Typeid);
        adduserscreen = (RelativeLayout) findViewById(R.id.adduserscreen);


        final String nameStr = name.getText().toString().trim();
        final String passwordStr = password.getText().toString().trim();
        final String hostelStr = Hostel.getText().toString().trim();
        final String type = Type.getText().toString().trim();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, urlLogin,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        //Toast.makeText(Login.this, response, Toast.LENGTH_SHORT).show();

                        if (response.toString().trim().equals("success")) {

                            Intent intent = new Intent(adduser.this, whatsappview.class);
                            startActivity(intent);


                        } else {
                            snackBarError("Wrong entry", "Retry", adduserscreen);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(adduser.this, error.toString(), Toast.LENGTH_SHORT).show();
                        snackBarError("Network Error", "Retry", adduserscreen);
                        Log.e("VOLLEY", "AuthFailureError");
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("userid", nameStr);
                params.put("pswd", passwordStr);
                params.put("type",type);
                params.put("hostel",hostelStr);


                return params;
            }
        };



        requestQueue.add(stringRequest);


    }

    ;



    public void snackBarError (String text,String action,RelativeLayout layout ){

        Snackbar snackbar = Snackbar
                .make(layout, text, Snackbar.LENGTH_LONG)
                .setAction(action, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        onClickedUseraddbutton(view);

                    }
                });

        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(getResources().getColor(R.color.lightgreen));

        snackbar.show();
    }

}

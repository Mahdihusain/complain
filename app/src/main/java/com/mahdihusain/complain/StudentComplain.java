package com.mahdihusain.complain;

import android.util.Log;

import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class StudentComplain {
    private static StudentComplain instance;

    private StudentComplain()
    {

    }

    public static StudentComplain getInstance()
    {
        if (instance == null)
            instance = new StudentComplain();

        return instance;
    }


    String studentcomplains;

    ArrayList<String> titlelist = new ArrayList<String>();

    public ArrayList<String> getStudentTitlelist() throws  JSONException{
        JSONArray ja = parseString(StudentComplain.getInstance().getStudentcomplains());
        maketitle(ja);
        return this.titlelist;
    }

    public ArrayList<String> maketitle(JSONArray jsonArray) throws JSONException{

        for (int i = 0; i < jsonArray.length(); ++i) {
            JSONObject rec = jsonArray.getJSONObject(i);
            //int id = rec.getInt("id");
            String title = rec.getString("ComplainTitle");
            titlelist.add(title);
        }
        return this.titlelist;

    }

    public String getStudentcomplains() {
        return this.studentcomplains;
    }

    public void setStudentcomplains(String studentcomplains) {
        this.studentcomplains = studentcomplains;
    }


    public JSONArray parseString(String s) throws  JSONException{

        Json jsonObj = new Json();
        String json = jsonObj.removeSlash(s);
        JSONArray obj = new JSONArray(json);

        return obj;
    }
}
